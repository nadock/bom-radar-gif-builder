#!/bin/bash

# Script to create Rain Radar gif from Bureau of Meteorology images
# - gets files from BoM ftp
# - builds gif with Image Magick
# - sends gif to slack using botty stuff

# requires imageMagick: brew install imagemagick

NORMAL=$(tput sgr0)
GREEN=$(tput setaf 2)
RED=$(tput setaf 1)
MAGENTA=$(tput setaf 5)

_cleanup_frames () {
	rm -rf rain-frames/
	rm -rf full-frames/
}

_bom_radar_get_frames_build_gif () {
	DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
	cd $DIR
	
	# get frames from BOM ftp
	echo "${MAGENTA}Getting frames...${NORMAL}"
	mkdir rain-frames
	cd rain-frames/
	wget -r -q -nv ftp://ftp.bom.gov.au/anon/gen/radar/IDR644.T*
 
 	# error handling
	if [[ $? != 0 ]]
	then 
		cd ..
		rm -rf rain-frames/ 
		echo "${RED}Couldn't get files from ftp. Stopping...${NORMAL}"
		exit $?
	fi

	# collapse folder structure to bring frames to first dir
	cd ..
	find rain-frames/ -mindepth 2 -type f -exec mv -i '{}' rain-frames/ ';'


	# Delete all frames except for the last 6
	cd rain-frames/
	ls -tF | grep -v / | tail -n +7 | xargs rm --
	cd ..


	# convert frame images to have usable, numbered filenames
	cd rain-frames/
	i=1
	for file in *.png;
	do 
		j=$( printf "%04d" "$i" )
		mv "$file" "$j.png"; (( i++ ))
	done


	# create composite of each rain frame with both background and locations, add to /full-frames dir
	mkdir ../full-frames
	echo "${MAGENTA}Combining frames with background...${NORMAL}"
	i=1
	for file in *.png;
	do  
		j=$( printf "%04d" "$i" )
		/usr/local/bin/convert ../radar-background.png "$j".png ../radar-locations.png -flatten ../full-frames/frame-"$j".png

		# error handling
		if [[ $? != 0 ]]
		then 
			cd ..
			_cleanup_frames 
			echo "${RED}Failed building composites. Stopping...${NORMAL}"
			exit $?
		fi

		(( i++ )) 
	done


	# create gif using composite frames
	echo "${MAGENTA}Creating gif...${NORMAL}"
	cd ../full-frames/
	/usr/local/bin/convert -delay 25 -loop 0 *.png ../rain-radar-nodelay.gif

	# error handling
	if [[ $? != 0 ]]
	then 
		cd ..
		_cleanup_frames
		echo "${RED}Failed creating gif. Stopping...${NORMAL}"
		exit $?
	fi

	# add delay to last frame
	cd ..
	convert rain-radar-nodelay.gif \( +clone -set delay 100 \) +swap +delete rain-radar.gif
	
	# error handling
	if [[ $? != 0 ]]
	then 
		cd ..
		rm -rf rain-frames/
		rm -rf full-frames/ 
		echo "${RED}Failed adding delay to last frame. Stopping...${NORMAL}"
		exit $?
	fi

	rm rain-radar-nodelay.gif

	# remove individual frames
	_cleanup_frames
	
	echo "${GREEN}Done!${NORMAL}"
}

_bom_radar_get_frames_build_gif

# checks for cached gif so we don't build it too often
# _check_for_cached_gif () {
# 	# check date modified of rain-radar.gif
#	# if it's older than 10 minutes, build a new one.
# }


# # send it to slack
# _send_gif_to_slack () {
# 	# https://api.slack.com/methods/files.upload
# }